package com.digitalglassware.engine.controllers;

import com.digitalglassware.engine.data.FileInformation;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jim Garety on 26/04/2017.
 * For explanations of code see:
 * http://www.mkyong.com/java/jackson-2-convert-java-object-to-from-json/
 * http://stackoverflow.com/questions/6349421/how-to-use-jackson-to-deserialise-an-array-of-objects
 */

public class JsonTransformer {

    public static String objectToJson(Object obj) throws JsonProcessingException {
        return new ObjectMapper().writeValueAsString(obj);
    }

    public static ArrayList<FileInformation> jsonToObject(String jsonString) throws IOException {
        return new ObjectMapper().readValue(jsonString, new TypeReference<List<FileInformation>>() {
        });
    }
}