package com.digitalglassware.engine.controllers;

import com.sun.org.apache.xerces.internal.parsers.DOMParser;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.StringWriter;

/**
 * Created by Jim Garety on 21/04/2017.
 * Currently not used (apart from unit tests). If going to store xml as xml then will use this utility class, if xml stored as string then this will not be used.
 */
public class XmlTransformer {

    public static Document stringToXML(String key, String xml) {
        DOMParser parser = new DOMParser();
        Document doc = null;
        try {
            parser.parse(new InputSource(new java.io.StringReader(xml)));
            doc = parser.getDocument();
            doc.setDocumentURI(key);
        } catch (Exception e) {
            // handle SAXException
        }
        return doc;
    }


    public static String XmlToString(Document doc) {
        String output = null;
        try {
            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer transformer = tf.newTransformer();
            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            StringWriter writer = new StringWriter();
            transformer.transform(new DOMSource(doc), new StreamResult(writer));
            output = writer.getBuffer().toString().replaceAll("\n|\r", "");

        } catch (Exception e) {
            // handle Exception
        }

        return output;
    }
}
