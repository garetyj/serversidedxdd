package com.digitalglassware.engine.controllers;

import com.digitalglassware.engine.dao.FileDaoImpl;
import com.digitalglassware.engine.persistence.BucketFileManager;
import org.apache.log4j.Logger;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.security.GeneralSecurityException;

/**
 * Controller class for Post and Get Rest calls from the Android app
 */

@RestController
@EnableAutoConfiguration
public class RestHandler {

    //TODO the names in here mean it wont be uploaded from the emulator
    private static final String expectedJsonOutput = "[{\"name\":\"xmlstuff21\",\"description\":\"File data/data/james2.xml uploaded 2017-04-28T11:29:44.716+0000\"}," +
            "{\"name\":\"xmlstuff1\",\"description\":\"File data/data/james2.xml uploaded 2017-04-28T11:29:44.716+0000\"}," +
            "{\"name\":\"fileName3\",\"description\":\"File data/data/james2.xml uploaded 2017-04-28T11:29:44.716+0000\"}," +
            "{\"name\":\"fileName4\",\"description\":\"File data/data/james2.xml uploaded 2017-04-28T11:29:44.716+0000\"}]";

    private static final String expectedJsonOutput2 = "{\"name\":\"xmlstuff1\"}";

    private Logger logger = Logger.getLogger(RestHandler.class);
    private FileDaoImpl fileDaoImpl;

    public RestHandler() {
        fileDaoImpl = new FileDaoImpl();
    }

    @RequestMapping(value = "/upload/v1/users/{macAddress}/{labBook}/{fileName}", method = RequestMethod.POST)
    public String uploadXMLFile(@PathVariable(value = "macAddress") String macAddress, @PathVariable(value = "labBook") String labBook,
                                @PathVariable(value = "fileName") String fileName, HttpServletRequest webRequest) {
        if (logger.isInfoEnabled()) {
            logger.info("uploadXMLFile(@PathVariable(value = \"macAddress\") String macAddress, @PathVariable(value = \"labBook\") String labBook,\n" +
                    "@PathVariable(value = \"fileName\") String fileName, HttpServletRequest webRequest)");
        }
        return fileDaoImpl.storeXmlFile(macAddress, labBook, fileName, webRequest.getParameter("recording"), webRequest.getParameter("description"));
        // return expectedJsonOutput2;
        //TODO confirm what data is to be returned here
    }


    @RequestMapping(value = "/upload/v1/users/{macAddress}/{labBook}", method = RequestMethod.GET)
    public String getUploadedFiles2(@PathVariable(value = "macAddress") String macAddress, @PathVariable(value = "labBook") String labBook) {
        if (logger.isInfoEnabled()) {
            logger.info("Inside temp(@PathVariable(value=\"btMac\") String btMac, @PathVariable(value=\"labBook\") String labBook");
        }
        //TODO The Json is not working correctly in the Android App - needs done next

        try {
            String returnString = BucketFileManager.getInstance().getExistingFileInformation(macAddress + "/" + labBook);
            if (returnString.equals("")) {
                logger.info("Json String empty, setting as []");
                returnString = "[]";
            }
            logger.info("Going to return Json: " + returnString);
            return returnString;
            //  return expectedJsonOutput; //todo this is a placeholder need to correct the android code and return correct JSon
        } catch (IOException | GeneralSecurityException e) {
            if (logger.isErrorEnabled()) {
                logger.error("Inside temp(@PathVariable(value=\"btMac\") String btMac, @PathVariable(value=\"labBook\") String labBook");
            }
            return expectedJsonOutput;
        }
    }
}