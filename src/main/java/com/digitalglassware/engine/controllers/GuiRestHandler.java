package com.digitalglassware.engine.controllers;

import com.digitalglassware.engine.data.FileInformation;
import com.digitalglassware.engine.persistence.BucketFileManager;
import org.apache.log4j.Logger;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Controller class for Get Rest calls from the Browser
 */

@Controller
@EnableAutoConfiguration
public class GuiRestHandler {

    private Logger logger = Logger.getLogger(GuiRestHandler.class);

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String welcome(Map<String, Object> model) {
        if (logger.isInfoEnabled()) {
            logger.info("Inside getRootDirectoryMessage()");
        }
        model.put("message", "DeviceX is online");
        return "welcome";
    }

    //todo this is for dev purposes - it is returning the correct json but the andoid app cant handle it just now
    @RequestMapping(value = "/upload/v1/users/{macAddress}/{labBook}/gui", method = RequestMethod.GET)
    public String getTestUploadedFiles(Map<String, Object> model, @PathVariable(value = "macAddress") String macAddress, @PathVariable(value = "labBook") String labBook) {
        if (logger.isInfoEnabled()) {
            logger.info("Inside temp(@PathVariable(value=\"btMac\") String btMac, @PathVariable(value=\"labBook\") String labBook");
        }
        //TODO The Json is not working correctly in the Android App - needs done next
        List<FileInformation> data = new ArrayList<>();
        try {
            data = (BucketFileManager.getInstance().getExistingFileInformationAsList(macAddress + "/" + labBook));
        } catch (IOException | GeneralSecurityException e) {
            if (logger.isErrorEnabled()) {
                logger.error("Inside temp(@PathVariable(value=\"btMac\") String btMac, @PathVariable(value=\"labBook\") String labBook");
            }
        }
        model.put("message", "Uploaded Data");
        model.put("data", data);
        return "data";

    }
}