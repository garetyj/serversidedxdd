package com.digitalglassware.engine.data;

/**
 * Created by Jim Garety on 28/04/2017.
 */
public class FileInformation {
    private String name;
    private String description;

    //private no-arg constructor to allow Jackson to instantiate class when mapping Json array to FileInformation object
    public FileInformation() {
    }

    public FileInformation(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    //equals and hashCode needed for comparison in unit testing
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FileInformation that = (FileInformation) o;

        if (!name.equals(that.name)) return false;
        return description.equals(that.description);
    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + description.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "FileInformation{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}