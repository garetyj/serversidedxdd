package com.digitalglassware.engine.persistence;

import com.digitalglassware.engine.controllers.JsonTransformer;
import com.digitalglassware.engine.controllers.StorageSample;
import com.digitalglassware.engine.data.FileInformation;
import com.digitalglassware.engine.enums.FileKeys;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.api.services.storage.model.StorageObject;
import org.apache.log4j.Logger;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Jim Garety on 25/04/2017.
 */
public class BucketFileManager {

    private Logger logger = Logger.getLogger(BucketFileManager.class);
    private static BucketFileManager serverFileWriter = null;
    private static final String BUCKET_NAME = "james999333mmmnnnasdsdsdadsfdssadfassdfssdhsdsd";
    private static final String FILE_DIRECTORY_NAME = "FileInformationAsJson";


    public static BucketFileManager getInstance() {
        if (Logger.getLogger(BucketFileManager.class).isInfoEnabled()) {
            Logger.getLogger(BucketFileManager.class).info("Inside getInstance()");
        }
        if (serverFileWriter == null) {
            serverFileWriter = new BucketFileManager();
        }
        return serverFileWriter;
    }

    private BucketFileManager() {
        //private no argument constructor to enforce singleton design pattern
    }

    /**
     * TODO Refactor this method
     * @param fileDetails
     * @return
     */
    public String create(Map<String, String> fileDetails) {
        if (logger.isInfoEnabled()) {
            logger.info("Inside create(Map<String, String> file)");
        }

        String fileName = fileDetails.get(FileKeys.FILE_PATH.toString()) + fileDetails.get(FileKeys.FILE_NAME.toString());

        File tempFile = null;
        // Create a temp file to upload
        Path tempPath = null;

        Long startTime = System.currentTimeMillis();

        try {
            tempPath = Files.createTempFile("StorageSample", "txt");
            Files.write(tempPath, fileDetails.get(FileKeys.FILE_CONTENT.toString()).getBytes());
            tempFile = tempPath.toFile();
            // Upload it
            StorageSample.uploadFile(fileName + ".xml", "text/plain", tempPath.toFile(), BUCKET_NAME);
            tempFile.delete();
        } catch (IOException | GeneralSecurityException e) {
            e.printStackTrace();
        }

        try {
            tempPath = Files.createTempFile("StorageSample", "txt");
            Files.write(tempPath, fileDetails.get(FileKeys.FILE_DESCRIPTION.toString()).getBytes());
            tempFile = tempPath.toFile();
            // Upload it
            StorageSample.uploadFile(fileName + "_description.txt", "text/plain", tempFile, BUCKET_NAME);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (GeneralSecurityException e) {
            e.printStackTrace();
        }

        tempFile.delete();

        //todo uploading the json file - this whole class needs refactored.
        try {
            tempPath = Files.createTempFile("StorageSample", ".txt");
            Files.write(tempPath, updateJsonInformation(fileDetails.get(FileKeys.FILE_PATH.toString()), fileDetails.get(FileKeys.FILE_NAME.toString()), fileDetails.get(FileKeys.FILE_DESCRIPTION.toString())).getBytes());
            tempFile = tempPath.toFile();
            // Upload it
            StorageSample.uploadFile(fileDetails.get(FileKeys.FILE_PATH.toString()) + FILE_DIRECTORY_NAME, "text/plain", tempFile, BUCKET_NAME);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (GeneralSecurityException e) {
            e.printStackTrace();
        }

        tempFile.deleteOnExit();
        Long endTime = System.currentTimeMillis();
        Long finalTime = endTime - startTime;


        logger.info("Time taken = " + finalTime + " msecs");
        logger.info("Time taken = " + (finalTime / 1000) + " secs");

        tempFile.delete();
        String returnString = "";
        try {
            returnString = JsonTransformer.objectToJson(fileDetails);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return returnString;
    }

    /**
     * Static Utility method to parse contents of StorageObject from Google App Engine Media Link
     *
     * @param mediaLink
     * @return Contents of File as a String
     * @throws IOException
     * @throws MalformedURLException
     */
    private static String getFileContent(String mediaLink) throws IOException, MalformedURLException {
        BufferedInputStream in;
        String strFileContents = "";
        in = new BufferedInputStream(new URL(mediaLink).openStream());

        final byte data[] = new byte[1024];
        int count;

        while ((count = in.read(data, 0, 1024)) != -1) {
            strFileContents += new String(data, 0, count);
        }
        in.close();
        return strFileContents;
    }


    /**
     * returns the file names and descriptions as a key value pair as a Json String
     *
     * @param fileDirectory
     * @return
     * @throws IOException
     * @throws GeneralSecurityException
     */
    public String getExistingFileInformation(String fileDirectory) throws IOException, GeneralSecurityException {

        // List the contents of the bucket.
        List<StorageObject> bucketContents = StorageSample.listBucket(BUCKET_NAME);
        String existingFiles = "";
        for (StorageObject object : bucketContents) {
            if (object.getName().equals(fileDirectory + "/" + FILE_DIRECTORY_NAME)) {
                existingFiles = getFileContent(object.getMediaLink());
                break;
            }
        }

        return existingFiles;
    }

    public String updateJsonInformation(String filePath, String fileName, String fileDescription) throws IOException, GeneralSecurityException {
        List<StorageObject> bucketContents = StorageSample.listBucket(BUCKET_NAME);
        String fileInformationList = "";


        System.out.println("filePath: " + filePath);
        System.out.println("fileName: " + fileName);
        for (StorageObject object : bucketContents) {
            if (object.getName().equals(filePath + FILE_DIRECTORY_NAME)) {
                fileInformationList = getFileContent(object.getMediaLink());
                break;
            }
        }
        if (fileInformationList.equals("")) {
            fileInformationList = "[]";
        }

        fileInformationList = fileInformationList.replace("]", ",{\"name\": \"" + fileName + "\",\"description\":\"" + fileDescription + "\"}]");

        if (fileInformationList.startsWith("[,")) {
            fileInformationList = fileInformationList.replace("[,", "[");
        }

        return fileInformationList;
    }


    /**
     * Keeping for dev purposes just now this is to display a list of uploaded files and their descriptions.
     * This is slow as each '_description.txt' is being parsed and used to hydrate a FileInformation object.
     *
     * @param fileName
     * @return
     * @throws IOException
     * @throws GeneralSecurityException
     */
    public List<FileInformation> getExistingFileInformationAsList(String fileName) throws IOException, GeneralSecurityException {
        List<FileInformation> fileInformationList = new ArrayList<>();
        // List the contents of the bucket.
        List<StorageObject> bucketContents = StorageSample.listBucket(BUCKET_NAME);
        for (StorageObject object : bucketContents) {
            if (object.getName().endsWith("_description.txt")) {
                if (object.getName().startsWith(fileName)) {
                    fileInformationList.add(new FileInformation(object.getName().replaceAll("_description.txt", ""), getFileContent(object.getMediaLink())));
                }
            }

        }

        return fileInformationList;
    }

}
