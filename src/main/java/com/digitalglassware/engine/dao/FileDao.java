package com.digitalglassware.engine.dao;

/**
 * Created by Jim Garety on 21/04/2017.
 */
public interface FileDao<T> {
    T storeXmlFile(T macAddress, T labBook, T fileName, T xmlString, T description);
}