package com.digitalglassware.engine.dao;

import com.digitalglassware.engine.enums.FileKeys;
import com.digitalglassware.engine.persistence.BucketFileManager;
import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Jim Garety on 21/04/2017.
 */

public class FileDaoImpl implements FileDao<String> {

    private Logger logger = Logger.getLogger(FileDaoImpl.class);

    public String storeXmlFile(String macAddress, String labBook, String fileName, String xmlString, String description) {
        if (logger.isInfoEnabled()) {
            logger.info("Inside String macAddress, String labBook, String fileName, String xmlString, String description");
        }

        Map<String, String> fileDetailsMap = new HashMap<>();
        fileDetailsMap.put(FileKeys.FILE_PATH.toString(), macAddress + "/" + labBook + "/");
        fileDetailsMap.put(FileKeys.FILE_NAME.toString(), fileName);
        fileDetailsMap.put(FileKeys.FILE_DESCRIPTION.toString(), description);
        fileDetailsMap.put(FileKeys.FILE_CONTENT.toString(), xmlString);
        return BucketFileManager.getInstance().create(fileDetailsMap).toString();
    }

}