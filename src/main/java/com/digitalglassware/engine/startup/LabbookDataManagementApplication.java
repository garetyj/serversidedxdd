package com.digitalglassware.engine.startup;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.embedded.ConfigurableEmbeddedServletContainer;
import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainerFactory;
import org.springframework.context.annotation.Bean;

//for use of  scanBasePackageClasses see http://stackoverflow.com/questions/37130111/springbootapplication-in-same-package
//@SpringBootApplication(scanBasePackageClasses = {com.digitalglassware.RestHandler.class, WebSecurityConfig.class, MvcConfig.class})
@SpringBootApplication(scanBasePackageClasses = {com.digitalglassware.engine.controllers.RestHandler.class, com.digitalglassware.engine.controllers.GuiRestHandler.class})
public class LabbookDataManagementApplication {

    public static void main(String[] args) {
        SpringApplication.run(LabbookDataManagementApplication.class, args);
    }

    @Bean
    EmbeddedServletContainerCustomizer containerCustomizer() throws Exception {

        return (ConfigurableEmbeddedServletContainer container) -> {

            if (container instanceof TomcatEmbeddedServletContainerFactory) {

                TomcatEmbeddedServletContainerFactory tomcat = (TomcatEmbeddedServletContainerFactory) container;
                tomcat.addConnectorCustomizers(
                        (connector) -> {
                            connector.setMaxPostSize(100000000);//100MB
                        }
                );
            }
        };
    }

}
