package com.digitalglassware.engine.enums;

/**
 * Created by Jim Garety on 26/04/2017.
 */
public enum FileKeys {
    FILE_PATH("filePath"),
    FILE_NAME("fileName"),
    FILE_DESCRIPTION("fileDescription"),
    FILE_CONTENT("fileContent");

    private final String keyText;

    FileKeys(final String keyText) {
        this.keyText = keyText;
    }

    @Override
    public String toString() {
        return keyText;
    }
}