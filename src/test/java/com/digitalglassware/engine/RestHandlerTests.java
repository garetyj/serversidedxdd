package com.digitalglassware.engine;

import com.digitalglassware.engine.controllers.RestHandler;
import com.digitalglassware.engine.controllers.GuiRestHandler;
import org.springframework.web.context.WebApplicationContext;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration
public class RestHandlerTests {

    @Autowired
    private WebApplicationContext wac;
    private MockMvc mockMvc;

    @Before
    public void setUp() throws Exception {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
    }

    /**
     * Test to check the Rest call to non-existent URL returns a 404
     *
     * @throws Exception
     */
    @Test
    public void testGetIncorrectUrl() throws Exception {
        mockMvc.perform(get("/doesNotExist")).andExpect(status().is(404));
    }

    /**
     * Test to check the Rest call to /upload/v1/users/macAddress/labBook is functioning
     *
     * @throws Exception
     */
    @Test
    public void getWelcomePage() throws Exception {
        mockMvc.perform(get("/")).andExpect(status().is(200));
    }

    /**
     * Test to check the Rest call to /upload/v1/users/macAddress/labBook is functioning
     *
     * @throws Exception
     */
    @Test
    public void getFileAsList() throws Exception {
        mockMvc.perform(get("/upload/v1/users/macAddress/labBook/gui")).andExpect(status().is(200));
    }

    /**
     * Test to check the Rest call to /upload/v1/users/macAddress/labBook is functioning
     *
     * @throws Exception
     */
    @Test
    public void getFiles() throws Exception {
        mockMvc.perform(get("/upload/v1/users/macAddress/labBook")).andExpect(status().is(200));
    }

    @Test
    public void postFiles() throws Exception {
        mockMvc.perform(post("/upload/v1/userspost/macAddress/labBook/fileName").contentType("text/plain")
                .param("recording", "some XML")
                .param("description", "some description")).andExpect(status().is(200));
    }

    @Configuration
    @EnableAutoConfiguration
    public static class Config {
        @Bean
        public RestHandler restHandler() {
            return new RestHandler();
        }

        @Bean
        public GuiRestHandler guiRestHandler() {
            return new GuiRestHandler();
        }
    }


}
