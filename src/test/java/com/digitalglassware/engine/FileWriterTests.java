package com.digitalglassware.engine;

import com.digitalglassware.engine.controllers.JsonTransformer;

import com.digitalglassware.engine.data.FileInformation;
import com.digitalglassware.engine.enums.FileKeys;
import com.digitalglassware.engine.persistence.BucketFileManager;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration
public class FileWriterTests {

    private static final String expectedJsonOutput = "[{\"name\":\"fileName1\",\"description\":\"description1\"}," +
            "{\"name\":\"fileName2\",\"description\":\"description2\"}," +
            "{\"name\":\"fileName3\",\"description\":\"description3\"}," +
            "{\"name\":\"fileName4\",\"description\":\"description4\"}]";


    private static final String fileDescription = "This is a description for the file being uploaded";
    private static String mockBtMac = "001B44113AB";
    private static String mockLabBookName = "Cronin3D";
    private static Map<String, String> fileData;


    @Rule
    public TemporaryFolder tempFolder = new TemporaryFolder();

    /**
     * Unit Test to ensure the xml file and its directory are being created.
     * This creates 3 different directories (<root>/<macAddress>/<labBook></labBook>/) and creates an individual file in each.
     * The use of the TemporaryFolder ensures the files are deleted after the unit test has run.
     *
     * @throws Exception
     */
    @Test
    public void testFileWriter() throws Exception {
        Map<String, String> fileInformation;
        String createdFileDetails = null;
        int x;
        for (x = 0; x < 3; x++) {
            tempFolder.newFolder(mockBtMac + x, mockLabBookName + x);
            fileInformation = new HashMap<>();
            fileInformation.put(FileKeys.FILE_PATH.toString(), tempFolder.getRoot().getPath() + "\\" + mockBtMac + x + "\\" + mockLabBookName + x);
            fileInformation.put(FileKeys.FILE_NAME.toString(), String.valueOf(new Date().getTime()) + ".xml");
            fileInformation.put(FileKeys.FILE_DESCRIPTION.toString(), fileDescription);
            fileInformation.put(FileKeys.FILE_CONTENT.toString(), "xmlString");
            createdFileDetails = BucketFileManager.getInstance().create(fileInformation);
            assertNotNull(createdFileDetails);
        }
        //There should be 3 (the value of x) folders in the tempFolderRoot
        assertEquals(x, tempFolder.getRoot().listFiles().length);
        //Testing that the last file created in the temp folder root is a directory.
        assertEquals(true, tempFolder.getRoot().listFiles()[x - 1].isDirectory());
    }

    @Test
    public void testGetExistingFilesAsJson() throws Exception {
        List<FileInformation> fileInformationList = new ArrayList<>();
        fileInformationList.add(new FileInformation("fileName1", "description1"));
        fileInformationList.add(new FileInformation("fileName2", "description2"));
        fileInformationList.add(new FileInformation("fileName3", "description3"));
        fileInformationList.add(new FileInformation("fileName4", "description4"));

        String filesInformationAsJson = JsonTransformer.objectToJson(fileInformationList);
        System.out.println(filesInformationAsJson);
        assertEquals(expectedJsonOutput, filesInformationAsJson);
    }

    @Test
    public void testGetExistingFileDataFromJson() throws Exception {
        List<FileInformation> fileInfoList = JsonTransformer.jsonToObject(expectedJsonOutput);
        assertEquals(4, fileInfoList.size());
        assertEquals("fileName1", fileInfoList.get(0).getName());
    }

}