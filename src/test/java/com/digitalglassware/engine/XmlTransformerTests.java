package com.digitalglassware.engine;

import com.digitalglassware.engine.controllers.XmlTransformer;
import org.junit.Test;
import org.w3c.dom.Document;

import static org.junit.Assert.assertEquals;

/**
 * Created by Jim Garety on 08/05/2017.
 */
public class XmlTransformerTests {

    private static final String xml = "<data>" +
            "<expcode>cronin-ex-102</expcode>" +
            "<measurements>" +
            "<timestamp>1500</timestamp>" +
            "<temp>56</temp>" +
            "</measurements>" +
            "</data>";

    /**
     * Test to confirm that when XmlTransformer generates an XML document from an XML String and then back again, the original XML string is the same as the generated one
     *
     * @throws Exception
     */
    @Test
    public void testXmlTransformer() throws Exception {
        Document doc = XmlTransformer.stringToXML("key", xml);
        String processedXml = XmlTransformer.XmlToString(doc);
        assertEquals(xml, processedXml);
    }
}
